#!/bin/bash

set -uex

SUBSCRIPTION=$1
shift

testvm () {
  SIZE=${1:-Standard_D2_v2}
  VM="deleteme-$(echo $SIZE | sed -e 's/.*/\L&/' -e 's/standard_//' -e 's/_/-/g')"
  
  docker-machine rm -y $VM || true
  /usr/bin/time --verbose docker-machine create \
    --driver azure \
    --azure-subscription-id="$SUBSCRIPTION" \
    --azure-ssh-user=gitlab \
    --azure-image=Debian:debian-10:10:latest \
    --azure-no-public-ip \
    --azure-size="${SIZE}" \
    --azure-storage-type=Standard_LRS \
    --azure-location=westeurope \
    --azure-resource-group=lab000040-rg \
    --azure-vnet=lab000040-rg:lab000040-vnet \
    --azure-subnet=labsubnet \
    "${VM}" > azurevmperf-${SIZE}-create.log 2>&1
  
  eval $(docker-machine env ${VM})
  
  /usr/bin/time --verbose timeout 1h docker run \
    --volume "/mnt/resource/cache:/cache" \
    --volume "/mnt/resource/builds:/builds" \
    --cap-add=SYS_PTRACE \
    --tmpfs='/scratch:rw,exec' \
    registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2021dev2-image-builder \
    sh -c \
    'cd /builds && git clone https://gitlab-apertispro.boschdevcloud.com/iae1kor/apertis-image-recipes.git && cd apertis-image-recipes && debos --show-boot apertis-ospack-minimal.yaml' \
    > azurevmperf-${SIZE}-build.log 2>&1
  
  timeout 10m /usr/bin/time --verbose docker-machine rm -y "${VM}"
}

# Generated with 
# docker run mcr.microsoft.com/azure-cli az vm list-sizes -l westeurope --query '[?memoryInMb > `16000` && memoryInMb < `90000` && resourceDiskSizeInMb > `30000` && resourceDiskSizeInMb < `90000`].name' -o tsv
SIZES="
  Standard_B4ms
  Standard_B8ms
  Standard_D4a_v4
  Standard_DS4_v2
  Standard_DS12-1_v2
  Standard_DS12-2_v2
  Standard_DS12_v2
  Standard_DS11_v2
  Standard_DS11-1_v2
  Standard_F8s
  Standard_F16s
  Standard_A4m_v2
  Standard_A8m_v2
  Standard_A8_v2
  Standard_D4s_v3
  Standard_D8s_v3
  Standard_E2a_v4
  Standard_E4a_v4
  Standard_E2_v3
  Standard_E2s_v3
  Standard_E4-2s_v3
  Standard_E4s_v3
  Standard_F8s_v2
  Standard_D2_v2
  Standard_D3_v2
  Standard_D2_v3
  Standard_D4_v3
  Standard_D8_v3
  Standard_L4s
  "

for S in $SIZES
do
  testvm "$S" &
  sleep 1
done

wait

